/*Consider the leftmost and righmost appearances of some value in an array. 
We'll say that the "span" is the number of elements between the two inclusive. 
A single value has a span of 1. Returns the largest span found in the given array. 
(Efficiency is not a priority.)*/

/*This algorithm will solve all cases in O(n^2) because there is a nested for 
  loop. We could make this more efficient for large arrays by storing a sorted
  copy of the array, and checking if we had already looked at the span
  of a given value. Although process of sorting and checking would still
  consume some time and memory. 
  
  If we were working with many arrays of size 1 or 0 we may want to consider:
    if (nums.length == 0) {return 0;}
    if (nums.length == 1) {return 1;}

my solution: */
public int maxSpan(int[] nums) {
  int maxSpan = 0;
  int leftNumberIndex = 0;
  int rightMostIndex = 0;
  for (int i = 0; i < nums.length; i++) {
    leftNumberIndex = nums[i];
    for (int j = i; j < nums.length; j++) {
      if(nums[i]==nums[j]) {
        rightMostIndex = j;
        int tempSpan = (j - i) + 1;
        if (tempSpan > maxSpan) {
          maxSpan = tempSpan;
        }
      }  
    }
  }
  return maxSpan;
}
