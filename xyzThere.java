//Return true if the given string contains an appearance of "xyz" 
//where the xyz is not directly preceeded by a period (.). 
//So "xxyz" counts but "x.xyz" does not.

public boolean xyzThere(String str) {
String search = "";
int xyzCount = 0;
int dotXyzCount = 0;
for (int i = 0; i < str.length()-2; i++) {
  if (str.substring(i, i+3).equals("xyz")) {
    xyzCount++;
  }  
}
for (int i = 0; i < str.length()-3; i++) {
    if (str.substring(i, i+4).equals(".xyz")) {
    dotXyzCount++;
  }
}
return xyzCount - dotXyzCount > 0; 
}